#!/usr/bin/bash
inputfile=$1
outputfile=$2
grep "id='storytext'" $inputfile \
| sed \
      -e "s#<div class='storytext xcontrast_txt nocopy' id='storytext'>##g" \
      -e "s#<p style='text-align:center;'><strong>\([^<]\+\)</strong></p><p style='text-align:center;'><strong>\([^<]\+\)</strong>#= \1 \2#" \
      -e "s#</p><p style='text-align:center;'>- break -#\\n\\n***\\n\\n#g" \
      -e 's#</p><p>#\n\n#g' \
      -e "s#</p><p style='text-align:center;'>#\n\n(centered) #g" \
      -e 's#</\?em>#__#g' \
      -e 's#</\?strong>#**#g' \
      -e 's#</p>##' \
      -e "s#<p style='text-align:center;'><strong>\([^<]\+\)</strong></p><p style='text-align:center;'><strong>\([^<]\+\)</strong>#= \1 \2#" \
      -e "s#<p style='text-align:center;'>#(centered)#g" \
> $outputfile;
#vim -c ':v/^$/normal I//' -c ':normal ggVGgq' -c ':wq' $outputfile;
      #-e 's!\[!\&#91;!g' \
      #-e 's!\]!\&#93;!g' \


# :set textwidth=80
# :argdo normal gg
# :argdo v/^$/normal I//
# :argdo g/^$/normal o// a0"ayiw
