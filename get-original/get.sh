#!/bin/bash
set -ex
for i in $(seq 1 108) ; do
    wget "https://www.fictionpress.com/s/2961893/$i/Mother-of-Learning" -O $i.html;
    sleep 1;
done
for i in 2 10 12 14 17 18 19 20 24 28 32 33 34 36 39 40 41 42 45 46 49 50 52 54 55 57 58 59 61 68 70 71 74 75 77 78 80 81 83 84 88 93 95 96 100 107 ; do
    mv $i.html $i.html.gz;
    gunzip $i.html.gz;
done
for i in $(seq 1 9) ; do
    mv -v $i.html 00$i.html;
done
for i in $(seq 10 99) ; do
    mv -v $i.html 0$i.html;
done
